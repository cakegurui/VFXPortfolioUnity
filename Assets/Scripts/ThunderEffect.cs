using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThunderEffect : MonoBehaviour
{
    public Animator myAnimator;
    public List<ParticleSystem> myParticles;

    public Transform myTransform;
    
   [SerializeField]
    GameObject thunderObject;
    [SerializeField,Range(1,6)]
    private int thunderAmount;
    // Start is called before the first frame update
    void Start()
    {
    
       myAnimator = gameObject.GetComponent<Animator>();
      // myAnimator.enabled=false;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Jump")){
            
           //myAnimator.enabled=true;
         StartCoroutine(PlayThunderStormRoutine());
        }


    IEnumerator PlayThunderStormRoutine(){

         
         int totalThunders = thunderAmount;// Random.Range(thunderAmount,thunderAmount+2);
         float totalTime =0;
         float t =5.0f;
    while(totalTime <t){
          
           for (int i = 0; i < totalThunders; i++)
        {
            totalTime += Time.time;
            //Debug.Log("time delay t :"+t);

            Vector3 instancePos= myTransform.position + 
            new Vector3(Mathf.Floor(myTransform.position.x+Random.Range(-8.0f,8.0f)),
            myTransform.position.y,
            Mathf.Ceil(myTransform.position.z+Random.Range(-6.0f,6.0f)));
            // yield return new WaitForEndOfFrame();
            yield return new WaitForSeconds(0.1f);
             GameObject thunderInstance= Instantiate(thunderObject,instancePos,Quaternion.identity);
             float animDuration = thunderInstance.GetComponent<Animator>().GetCurrentAnimatorClipInfo(0).Length;
             //start added code
             float delayanim =0.35f;
              animDuration +=delayanim;

              for (int j = 0; j < myParticles.Count; j++)
              { 
                
               float part= myParticles[j].main.startDelay.constant; 
               part*=delayanim;
               animDuration+= part;
               
              }
              //end added code
              Debug.Log(animDuration);
              thunderInstance.GetComponent<Animator>().Play("mainthunderAnim"); 
          
           if(totalTime>=animDuration)
           {    

                t=0.0f;
            yield return new WaitForSeconds(0.2f);
                Destroy(thunderInstance,animDuration);
                
           }
                    
        }
   
    }
    }
    
    void PlayThunderAnimation(){
        
      //   if (myAnimator.isActiveAndEnabled)
        {     
                Debug.Log("animation played!!");
                // myAnimator.SetTrigger("thunderOn");
                 thunderObject.GetComponent<Animator>().SetBool("thunderOn",true);
                thunderObject.GetComponent<Animator>().Play("mainthunderAnim");
               thunderObject.GetComponent<Animator>().SetBool("thunderOn",false);
    
        }
    }
    }
}
